package com.milopong.monolink.eventmanage.presentation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.milopong.monolink.eventmanage.business.EventManager;
import com.milopong.monolink.eventmanage.common.Event;

import net.sf.json.JSONObject;

@Controller
public class SelectEventControl {

	@Autowired
	EventManager eventManager;

	JSONObject json;

	List<Event> events;

	@RequestMapping("selectEvent.do")
	public @ResponseBody JSONObject selectEvent() {
		json = new JSONObject();

		events = new ArrayList<Event>();

		events = eventManager.selectAll();
		json.put("events", events);

		return json;

	}

}
