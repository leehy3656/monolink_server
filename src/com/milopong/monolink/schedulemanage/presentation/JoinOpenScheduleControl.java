package com.milopong.monolink.schedulemanage.presentation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.milopong.monolink.membermanage.business.MemberManager;
import com.milopong.monolink.membermanage.common.Member;
import com.milopong.monolink.memberschdulemanage.business.MemberScheduleManager;
import com.milopong.monolink.memberschdulemanage.common.MemberSchedule;
import com.milopong.monolink.schedulemanage.business.ScheduleManager;
import com.milopong.monolink.schedulemanage.common.Schedule;

import net.sf.json.JSONObject;

@Controller
public class JoinOpenScheduleControl {

	@Autowired
	MemberManager memberManager;
	@Autowired
	ScheduleManager scheduleManager;
	@Autowired
	MemberScheduleManager memberScheduleManager;

	private JSONObject json = null;
	private Member member = null;
	private Schedule schedule = null;
	private MemberSchedule memberSchedule = null;
	private List<MemberSchedule> memberSchedules = null;
	private List<Member> participants = null;

	@RequestMapping("joinOpenSchedule.do")
	public @ResponseBody JSONObject joinOpenSchedule(String sNo, String email) {
		json = new JSONObject();
		member = memberManager.selectByEmail(email);

		int no = Integer.valueOf(sNo);
		schedule = scheduleManager.selectByNo(no);

		memberSchedule = new MemberSchedule();
		memberSchedule.setMember(member);
		memberSchedule.setSchedule(schedule);

		memberScheduleManager.regist(memberSchedule);

		Schedule newSchedule = new Schedule();
		try {
			newSchedule = schedule.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		newSchedule.setRootSchedule(schedule.getNo());
		newSchedule.setStatus("guest");
		newSchedule.setNotification("no");
		newSchedule.setMember(member);
		scheduleManager.regist(newSchedule);
		
		memberSchedules = new ArrayList<MemberSchedule>();
		memberSchedules = memberScheduleManager.selectBySchedule(schedule);
		
		participants = new ArrayList<Member>();
		for (int i = 0; i < memberSchedules.size(); i++) {
			participants.add(memberSchedules.get(i).getMember());
		}
		participants.add(schedule.getMember());
		for(int i=0; i<participants.size(); i++){
			if(participants.get(i).getEmail().equals(member.getEmail())){
				participants.remove(i);
			}
		}
		
		
		for(int i=0; i<participants.size(); i++){
			MemberSchedule aMemberSchedule = new MemberSchedule();
			aMemberSchedule.setMember(participants.get(i));
			aMemberSchedule.setSchedule(newSchedule);
			memberScheduleManager.regist(aMemberSchedule);
		}
		json.put("status","success");
		return json;
	}
}
