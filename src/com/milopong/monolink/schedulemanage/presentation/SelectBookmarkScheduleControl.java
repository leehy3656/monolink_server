package com.milopong.monolink.schedulemanage.presentation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.milopong.monolink.membermanage.business.MemberManager;
import com.milopong.monolink.membermanage.common.Member;
import com.milopong.monolink.memberschdulemanage.business.MemberScheduleManager;
import com.milopong.monolink.memberschdulemanage.common.MemberSchedule;
import com.milopong.monolink.schedulemanage.business.ScheduleManager;
import com.milopong.monolink.schedulemanage.common.Schedule;

import net.sf.json.JSONObject;

@Controller
public class SelectBookmarkScheduleControl {

	@Autowired
	private MemberManager memberManager;
	@Autowired
	private ScheduleManager scheduleManager;
	@Autowired
	private MemberScheduleManager memberScheduleManager;
	
	private JSONObject json = null;
	private List<Schedule> bookmarkSchedule =null;
	private Member member = null;
	
	@RequestMapping("selectBookmarkSchedule.do")
	public @ResponseBody JSONObject selectBookmarkSchedule(String email){
		
		json = new JSONObject();
		bookmarkSchedule = new ArrayList<Schedule>();
		member = new Member();
		
		member = memberManager.selectByEmail(email);
		
		bookmarkSchedule = scheduleManager.selectByBookmarkSchedule(member);
		
		for(int i=0; i < bookmarkSchedule.size(); i++){
			List<MemberSchedule> memberSchedule = new ArrayList<MemberSchedule>();
			
			memberSchedule = memberScheduleManager.selectBySchedule(bookmarkSchedule.get(i));
			
			List<Member> participants = new ArrayList<>();
			for(int j=0; j<memberSchedule.size(); j++){
				Member aMember = new Member();
				aMember = memberSchedule.get(j).getMember();
				participants.add(aMember);
			}
			int scheduleNo = bookmarkSchedule.get(i).getNo();
			
			System.out.println("participants Size:"+participants.size());
			json.put("participants"+scheduleNo, participants);
		}
		
		
		json.put("schedules", bookmarkSchedule);
		return json;
		
	}
}
