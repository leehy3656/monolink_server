package com.milopong.monolink.schedulemanage.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.milopong.monolink.schedulemanage.business.ScheduleManager;
import com.milopong.monolink.schedulemanage.common.Schedule;

import net.sf.json.JSONObject;

@Controller
public class UpdateScheduleControl {
	@Autowired
	ScheduleManager scheduleManager;

	private JSONObject json = null;
	private Schedule schedule = null;

	@RequestMapping("updateStatus.do")
	public @ResponseBody JSONObject updateStatus(String sNo) {
		int no = Integer.parseInt(sNo);
		schedule = new Schedule();
		schedule = scheduleManager.selectByNo(no);
		schedule.setStatus("guest");
		scheduleManager.update(schedule);
		json = new JSONObject();
		json.put("status", "success");
		return json;

	}

	@RequestMapping("registBookmark.do")
	public @ResponseBody JSONObject registBookmark(String sNo) {
		int no = Integer.parseInt(sNo);
		schedule = new Schedule();
		schedule = scheduleManager.selectByNo(no);
		schedule.setBookmark("true");
		scheduleManager.update(schedule);
		json = new JSONObject();
		json.put("status", "success");
		return json;

	}

	@RequestMapping("unRegistBookmark.do")
	public @ResponseBody JSONObject updateBookmark(String sNo) {
		int no = Integer.parseInt(sNo);
		schedule = new Schedule();
		schedule = scheduleManager.selectByNo(no);
		schedule.setBookmark(null);
		scheduleManager.update(schedule);
		json = new JSONObject();
		json.put("status", "success");
		return json;

	}

}
