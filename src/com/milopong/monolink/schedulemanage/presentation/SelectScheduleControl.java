package com.milopong.monolink.schedulemanage.presentation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.milopong.monolink.membermanage.business.MemberManager;
import com.milopong.monolink.membermanage.common.Member;
import com.milopong.monolink.memberschdulemanage.business.MemberScheduleManager;
import com.milopong.monolink.memberschdulemanage.common.MemberSchedule;
import com.milopong.monolink.schedulemanage.business.ScheduleManager;
import com.milopong.monolink.schedulemanage.common.Schedule;

import net.sf.json.JSONObject;

@Controller
public class SelectScheduleControl {

	@Autowired
	private ScheduleManager scheduleManager;
	@Autowired
	private MemberManager memberManager;
	@Autowired
	private MemberScheduleManager memberScheduleManager;

	private Member member = null;
	private JSONObject json = null;
	private List<Schedule> schedules = null;
	private Schedule schedule = null;

	@RequestMapping("selectSchedule.do")
	@ResponseBody
	JSONObject selectSchedule(String email) {
		json = new JSONObject();
		schedules = new ArrayList<Schedule>();
		member = memberManager.selectByEmail(email);
		schedules = scheduleManager.selectByMember(member);
		for (int i = 0; i < schedules.size(); i++) {
			List<MemberSchedule> memberSchedule = new ArrayList<MemberSchedule>();
			memberSchedule = memberScheduleManager.selectBySchedule(schedules.get(i));
			List<Member> participants = new ArrayList<Member>();

			for (int j = 0; j < memberSchedule.size(); j++) {
				Member aMember = new Member();
				aMember = memberSchedule.get(j).getMember();
				participants.add(aMember);
			}
			int scheduleNo = schedules.get(i).getNo();
			json.put("participants" + scheduleNo, participants);
		}
		json.put("schedules", schedules);

		return json;

	}

	@RequestMapping("selectScheduleByNo.do")
	public @ResponseBody JSONObject selectScheduleByNo(String sNo) {
		int no = Integer.valueOf(sNo);
		json = new JSONObject();
		schedule = new Schedule();
		schedule = scheduleManager.selectByNo(no);
		List<MemberSchedule> memberSchedule = new ArrayList<MemberSchedule>();
		List<Member> participants = new ArrayList<Member>();

		for (int i = 0; i < memberSchedule.size(); i++) {
			Member aMember = new Member();
			aMember = memberSchedule.get(i).getMember();
			participants.add(aMember);
		}

		json.put("participants", participants);
		json.put("schedule", schedule);

		return json;
	}
	
	@RequestMapping("selectTodaySchedule.do")
	@ResponseBody
	JSONObject selectTodaySchedule(String email) {
		json = new JSONObject();
		schedules = new ArrayList<Schedule>();
		member = memberManager.selectByEmail(email);
		schedules = scheduleManager.selectByMemberToday(member);
		
		for (int i = 0; i < schedules.size(); i++) {
			List<MemberSchedule> memberSchedule = new ArrayList<MemberSchedule>();
			memberSchedule = memberScheduleManager.selectBySchedule(schedules.get(i));
			List<Member> participants = new ArrayList<Member>();

			for (int j = 0; j < memberSchedule.size(); j++) {
				Member aMember = new Member();
				aMember = memberSchedule.get(j).getMember();
				participants.add(aMember);
			}
			int scheduleNo = schedules.get(i).getNo();
			json.put("participants" + scheduleNo, participants);
		}
		json.put("schedules", schedules);

		return json;

	}
}
