package com.milopong.monolink.schedulemanage.dataservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.milopong.monolink.friendmanage.common.Friend;
import com.milopong.monolink.membermanage.common.Member;
import com.milopong.monolink.schedulemanage.common.Schedule;

@Repository
public class ScheduleDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public Schedule selectByNo(int no) {
		Session session = sessionFactory.getCurrentSession();
		Schedule schedule = (Schedule) session.get(Schedule.class, new Integer(no));

		return schedule;
	}

	public void update(Schedule schedule) {
		Session session = sessionFactory.getCurrentSession();
		session.update(schedule);
	}

	public int regist(Schedule schedule) {
		Session session = sessionFactory.getCurrentSession();
		return (Integer) session.save(schedule);

	}

	public List<Schedule> selectByMember(Member member) {
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(Schedule.class).add(Restrictions.eq("member", member));
		List<Schedule> schedules = criteria.list();
		return schedules;
	}

	public List<Schedule> selectByMemberToday(Member member) {
		Session session = sessionFactory.getCurrentSession();
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
		Date currentTime = new Date();
		String mTime = mSimpleDateFormat.format(currentTime);
		System.out.println("mTime:" + mTime);
		Criteria criteria = session.createCriteria(Schedule.class).add(Restrictions.eq("member", member))
				.add(Restrictions.like("startTime", mTime + "%"));
		List<Schedule> schedules = criteria.list();
		return schedules;
	}

	public List<Schedule> selectByRootSchedule(int no) {
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(Schedule.class).add(Restrictions.eq("rootSchedule", no));
		List<Schedule> schedules = criteria.list();
		return schedules;
	}

	public List<Schedule> selectByOpenSchedule(List<Friend> friendObjects) {
		Session session = sessionFactory.getCurrentSession();
		List<Schedule> openSchedules = new ArrayList<Schedule>();

		for (int i = 0; i < friendObjects.size(); i++) {
			Criteria criteria = session.createCriteria(Schedule.class)
					.add(Restrictions.eq("member", friendObjects.get(i).getFriend()))
					.add(Restrictions.eq("scope", "public")).add(Restrictions.eq("rootSchedule", 0));
			List<Schedule> publicSchedules = new ArrayList<Schedule>();
			publicSchedules = criteria.list();
			openSchedules.addAll(publicSchedules);

			if (friendObjects.get(i).getStatus().equals("bilateral")) {
				Criteria criteria1 = session.createCriteria(Schedule.class)
						.add(Restrictions.eq("member", friendObjects.get(i).getFriend()))
						.add(Restrictions.eq("scope", "friend")).add(Restrictions.eq("rootSchedule", 0));
				List<Schedule> friendSchedules = new ArrayList<Schedule>();
				friendSchedules = criteria1.list();
				openSchedules.addAll(friendSchedules);
			}

		}
		return openSchedules;
	}

	public List<Schedule> selectByMemberNo(int memberNo) {

		Session session = sessionFactory.getCurrentSession();

		return session
				.createSQLQuery(
						"select a.* from schedule a left join memberschedule b on a.no=b.schedule_no where b.Member_no="
								+ memberNo + "union select a.* from schedule a where member_no=" + memberNo)
				.addEntity(Schedule.class).list();
	}

	public int delete(int no) {
		Session session = sessionFactory.getCurrentSession();
		Schedule schedule = (Schedule) session.get(Schedule.class, new Integer(no));

		session.delete(schedule);
		return 1;
	}

	public List<Schedule> selectByBookmarkSchedule(Member member) {
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(Schedule.class).add(Restrictions.eq("member", member))
				.add(Restrictions.eq("bookmark", "true"));
		List<Schedule> schedules = criteria.list();
		return schedules;

	}

	public List<Schedule> selectByFriendSchedule(Member member, String status) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = null;
		if (status.equals("bilateral")) {
			criteria = session.createCriteria(Schedule.class).add(Restrictions.eq("member", member))
					.add(Restrictions.in("scope", Arrays.asList("public", "friend")));
		} else if (status.equals("oneway")) {
			criteria = session.createCriteria(Schedule.class).add(Restrictions.eq("member", member))
					.add(Restrictions.eq("scope", "public"));
		}
		List<Schedule> schedules = criteria.list();

		return schedules;

	}

}
