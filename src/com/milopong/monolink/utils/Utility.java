package com.milopong.monolink.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

public class Utility {
	
	public static void sendGcmMessage(String id, String sMessage){
		
		Sender sender = new Sender("AIzaSyAfLkVbshOP_JONnT4VazTOkkFybZ-RhCE");
		System.out.println("REG id:"+id);
		String regId = id;

		Message message = new Message.Builder().addData("msg", sMessage).build();
		List<String> list = new ArrayList<String>();
		list.add(regId);
		MulticastResult multiResult;
		try {
			multiResult = sender.send(message, list, 5);
			if (multiResult != null) {
				List<Result> resultList = multiResult.getResults();
				for (Result result : resultList) {
					System.out.println(result.getMessageId());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
