package com.milopong.monolink.friendmanage.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.milopong.monolink.friendmanage.business.FriendManager;
import com.milopong.monolink.friendmanage.common.Friend;
import com.milopong.monolink.membermanage.business.MemberManager;
import com.milopong.monolink.membermanage.common.Member;

import net.sf.json.JSONObject;

@Controller
public class AddFriendControl {

	@Autowired
	MemberManager memberManager;
	@Autowired
	FriendManager friendManager;
	JSONObject json;

	@RequestMapping("addFriend.do")
	public @ResponseBody JSONObject addFriend(String userEmail, String friendEmail) {

		Member user, friend;
		Friend friendObject = new Friend();
		user = memberManager.selectByEmail(userEmail);
		friend = memberManager.selectByEmail(friendEmail);
		json = new JSONObject();

		friendObject.setUser(user);
		friendObject.setFriend(friend);

		int status = friendManager.addFriend(friendObject);

		if(status ==0){
			json.put("status", "fail");
		}else{
			json.put("status", "success");
		}

		return json;

	}

}
