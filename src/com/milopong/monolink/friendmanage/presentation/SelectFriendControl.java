package com.milopong.monolink.friendmanage.presentation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.milopong.monolink.friendmanage.business.FriendManager;
import com.milopong.monolink.friendmanage.common.Friend;
import com.milopong.monolink.membermanage.business.MemberManager;
import com.milopong.monolink.membermanage.common.Member;

import net.sf.json.JSONObject;

@Controller
public class SelectFriendControl {

	private JSONObject json = null;
	private Member member = null;
	private List<Friend> friends = null;
	@Autowired
	MemberManager memberManager;
	@Autowired
	FriendManager friendManager;

	@RequestMapping("selectFriend.do")
	public @ResponseBody JSONObject selectFriend(String email) {
		json = new JSONObject();
		System.out.println("Email:"+email);
		member = memberManager.selectByEmail(email);
		friends = friendManager.selectByUser(member);

		Collections.sort(friends, new NameAscCompare());
		
		json.put("friends", friends);
		

		return json;
	}
	
	static class NameAscCompare implements Comparator<Friend> {
		 
		/**
		 * 오름차순(ASC)
		 */
		@Override
		public int compare(Friend arg0, Friend arg1) {
			// TODO Auto-generated method stub
			return arg0.getFriend().getName().compareTo(arg1.getFriend().getName());
		}
 
	}

}
