package com.milopong.monolink.membermanage.presentation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.milopong.monolink.friendmanage.business.FriendManager;
import com.milopong.monolink.friendmanage.common.Friend;
import com.milopong.monolink.membermanage.business.MemberManager;
import com.milopong.monolink.membermanage.common.Member;

import net.sf.json.JSONObject;

@Controller
public class RecommendFriendControl {

	@Autowired
	MemberManager memberManager;
	@Autowired
	FriendManager friendManager;

	Gson gson = null;
	Member member = null;
	List<Member> monoMembers = null;
	List<Member> contacts = null;
	List<Friend> friendsObject = null;
	List<Member> friends = null;
	JSONObject json;

	@RequestMapping("recommendFriend.do")
	public @ResponseBody JSONObject recommendFriend(String members, String userEmail) {
		member = memberManager.selectByEmail(userEmail);

		gson = new Gson();
		monoMembers = new ArrayList<Member>();
		contacts = new ArrayList<Member>();
		friends = new ArrayList<Member>();
		monoMembers = memberManager.selectAll();
		friendsObject = friendManager.selectByUser(member);
		
		for (int i = 0; i < friendsObject.size(); i++) {
			Member friend = new Member();
			friend = friendsObject.get(i).getFriend();
			friends.add(friend);
		}
		
		contacts = gson.fromJson(members, new TypeToken<List<Member>>() {
		}.getType());

		json = new JSONObject();
		List<Member> test = new ArrayList<Member>();
		test= cmp(monoMembers, contacts, friends);
		json.put("recommend", test);
		
		return json;
	}

	private List<Member> cmp(List<Member> monoMembers, List<Member> contacts, List<Member> friends) {

		ArrayList<Member> recommend = new ArrayList<Member>();

		for (int i = 0; i < contacts.size(); i++) {
			for (int j = 0; j < monoMembers.size(); j++) {
				if (contacts.get(i).getPhone().replaceAll("-", "").equals(monoMembers.get(j).getPhone())) {
					recommend.add(monoMembers.get(j));
				}
			}
		}
		int recommendSize = recommend.size();
		for (int i = 0; i < recommendSize; i++) {
			for (int j = 0; j < friends.size(); j++) {
				if (recommend.get(i).getPhone().equals(friends.get(j).getPhone())) {
					recommend.remove(i);
					if (recommendSize != 1) {
						recommendSize--;
						i = -1;
					}

					break;
				}
			}
		}

		return recommend;

	}
}
